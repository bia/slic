/*
 * Copyright 2010-2016 Institut Pasteur.
 * 
 * This file is part of Icy.
 * 
 * Icy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Icy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Icy. If not, see <http://www.gnu.org/licenses/>.
 */
package algorithms.danyfel80.islic;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import icy.image.IcyBufferedImage;
import icy.roi.BooleanMask2D;
import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.type.DataType;
import icy.type.collection.array.Array2DUtil;
import icy.type.point.Point3D;
import plugins.kernel.roi.roi2d.ROI2DArea;

/**
 * @author Daniel Felipe Gonzalez Obando
 */
public class SLICTask
{

    private static final double EPSILON = 0.25;
    static int MAX_ITERATIONS = 10;
    static double ERROR_THRESHOLD = 1e-2;

    private Sequence sequence;
    private int S;
    private int Sx;
    private int Sy;
    private int SPnum;
    private double rigidity;

    private boolean computeROIs;
    private List<ROI> rois;
    private Sequence superPixelsResult;

    private Map<Point3D.Integer, double[]> colorLUT;
    private Map<Point, Double> distanceLUT;

    public SLICTask(Sequence sequence, int SPSize, double rigidity, boolean computeROIs)
    {
        this.sequence = sequence;
        this.S = SPSize;
        this.Sx = (sequence.getWidth() + S / 2 - 1) / S;
        this.Sy = (sequence.getHeight() + S / 2 - 1) / S;
        this.SPnum = Sx * Sy;
        this.rigidity = rigidity;

        this.computeROIs = computeROIs;

        colorLUT = new HashMap<>();
        distanceLUT = new HashMap<>();
    }

    public void execute()
    {

        double[] ls = new double[SPnum];
        double[] as = new double[SPnum];
        double[] bs = new double[SPnum];

        double[] cxs = new double[SPnum];
        double[] cys = new double[SPnum];

        double[][] sequenceData = Array2DUtil.arrayToDoubleArray(sequence.getDataXYC(0, 0),
                sequence.isSignedDataType());

        initialize(ls, as, bs, cxs, cys, sequenceData);

        int[] clusters = new int[sequence.getWidth() * sequence.getHeight()];
        double[] distances = Arrays.stream(clusters).mapToDouble(i -> Double.MAX_VALUE).toArray();

        int iterations = 0;
        double error = 0;
        do
        {
            assignClusters(sequenceData, clusters, distances, ls, as, bs, cxs, cys);
            error = updateClusters(sequenceData, clusters, ls, as, bs, cxs, cys);
            iterations++;
            System.out.format("Iteration=%d, Error=%f%n", iterations, error);
        }
        while (error > ERROR_THRESHOLD && iterations < MAX_ITERATIONS);
        System.out.format("End of iterations%n");

        computeSuperpixels(sequenceData, clusters, ls, as, bs, cxs, cys);
    }

    private void initialize(double[] ls, double[] as, double[] bs, double[] cxs, double[] cys, double[][] sequenceData)
    {

        IntStream.range(0, SPnum).forEach(sp -> {
            int x, y, yOff, bestx, besty, bestyOff, i, j, yjOff;
            double bestGradient, candidateGradient;

            int spx = sp % Sx;
            int spy = sp / Sx;

            x = S / 2 + (spx) * S;
            y = S / 2 + (spy) * S;
            yOff = y * sequence.getWidth();

            cxs[sp] = bestx = x;
            cys[sp] = besty = y;
            bestyOff = yOff;
            bestGradient = Double.MAX_VALUE;

            for (j = -1; j <= 1; j++)
            {
                if (y + j >= 0 && y + j < sequence.getHeight())
                {
                    yjOff = (y + j) * sequence.getWidth();
                    for (i = -1; i <= 1; i++)
                    {
                        if (x >= 0 && x < sequence.getWidth() && x + i >= 0 && x + i < sequence.getWidth())
                        {
                            candidateGradient = getGradient(sequenceData, x + yOff, (x + i) + yjOff);
                            if (candidateGradient < bestGradient)
                            {
                                bestx = x + i;
                                besty = y + j;
                                bestyOff = yjOff;
                                bestGradient = candidateGradient;
                            }
                        }
                    }
                }
            }

            cxs[sp] = bestx;
            cys[sp] = besty;
            int bestPos = bestx + bestyOff;

            double[] bestLAB = getCIELab(sequenceData, bestPos);
            ls[sp] = bestLAB[0];
            as[sp] = bestLAB[1];
            bs[sp] = bestLAB[2];

        });

    }

    private double getGradient(double[][] data, int pos1, int pos2)
    {
        int[] valRGB1 = new int[3];
        int[] valRGB2 = new int[3];
        IntStream.range(0, 3).forEach(c -> {
            try
            {
                valRGB1[c] = (int) Math.round(255 * (data[c % sequence.getSizeC()][pos1] / sequence.getDataTypeMax()));
                valRGB2[c] = (int) Math.round(255 * (data[c % sequence.getSizeC()][pos2] / sequence.getDataTypeMax()));
            }
            catch (Exception e)
            {
                throw e;
            }
        });

        double[] val1 = CIELab.fromRGB(valRGB1);
        double[] val2 = CIELab.fromRGB(valRGB2);

        double avgGrad = IntStream.range(0, 3).mapToDouble(c -> val2[c] - val1[c]).average().getAsDouble();

        return avgGrad;
    }

    private double[] getCIELab(double[][] sequenceData, int pos)
    {

        int[] rgbVal = new int[3];
        IntStream.range(0, 3).forEach(c -> {
            rgbVal[c] = (int) Math
                    .round(255 * (sequenceData[c % sequence.getSizeC()][pos] / sequence.getDataTypeMax()));
        });
        Point3D.Integer rgbPoint = new Point3D.Integer(rgbVal);

        double[] LAB = colorLUT.get(rgbPoint);
        if (LAB == null)
        {
            int[] RGB = new int[3];
            IntStream.range(0, 3).forEach(c -> {
                RGB[c] = (int) Math
                        .round(255 * (sequenceData[c % sequence.getSizeC()][pos] / sequence.getDataTypeMax()));
            });

            LAB = CIELab.fromRGB(RGB);
            colorLUT.put(rgbPoint, LAB);
        }
        return LAB;
    }

    private void assignClusters(double[][] sequenceData, int[] clusters, double[] distances, double[] ls, double[] as,
            double[] bs, double[] cxs, double[] cys)
    {
        // Each cluster
        IntStream.range(0, SPnum).forEach(k -> {
            double ckx = cxs[k], cky = cys[k], lk = ls[k], ak = as[k], bk = bs[k];
            int posk = ((int) ckx) + ((int) cky) * sequence.getWidth();
            distances[posk] = 0;
            clusters[posk] = k;

            // 2Sx2S window assignment
            int j, i, yOff;
            for (j = -S; j < S; j++)
            {
                int ciy = (int) (cky + j);
                if (ciy >= 0 && ciy < sequence.getHeight())
                {
                    yOff = ciy * sequence.getWidth();
                    for (i = -S; i < S; i++)
                    {
                        int cix = (int) (ckx + i);
                        if (cix >= 0 && cix < sequence.getWidth())
                        {
                            int posi = cix + yOff;
                            double[] LABi = getCIELab(sequenceData, posi);
                            double li = LABi[0];
                            double ai = LABi[1];
                            double bi = LABi[2];

                            double di = getDistance(ckx, cky, lk, ak, bk, i, j, li, ai, bi);
                            if (di < distances[posi])
                            {
                                distances[posi] = di;
                                clusters[posi] = clusters[posk];
                            }
                        }
                    }
                }
            }
        });
    }

    private double getDistance(double ckx, double cky, double lk, double ak, double bk, int dx, int dy, double li,
            double ai, double bi)
    {

        double diffl, diffa, diffb;
        diffl = li - lk;
        diffa = ai - ak;
        diffb = bi - bk;

        double dc = Math.sqrt(diffl * diffl + diffa * diffa + diffb * diffb);
        Point dPt = new Point((int) Math.min(dx, dy), (int) Math.max(dx, dy));
        Double ds = distanceLUT.get(dPt);
        if (ds == null)
        {
            ds = Math.sqrt(dx * dx + dy * dy);
            distanceLUT.put(dPt, ds);
        }
        return Math.sqrt(dc * dc + (ds * ds) * (rigidity * rigidity * S));
    }

    private double updateClusters(double[][] sequenceData, int[] clusters, double[] ls, double[] as, double[] bs,
            double[] cxs, double[] cys)
    {

        double[] newls = new double[SPnum];
        double[] newas = new double[SPnum];
        double[] newbs = new double[SPnum];
        double[] newcxs = new double[SPnum];
        double[] newcys = new double[SPnum];
        int[] cants = new int[SPnum];

        // int blockSize = 500;
        // IntStream.iterate(0, sy -> sy+blockSize).limit((int)Math.ceil(sequence.getHeight()/blockSize)).forEach(y0->{
        // int y1 = Math.min(y0+blockSize, sequence.getHeight() - y0);
        // IntStream.iterate(0, sx -> sx+blockSize).limit((int)Math.ceil(sequence.getWidth()/blockSize)).forEach(x0->{
        // int x1 = Math.min(x0+blockSize, sequence.getWidth() - x0);
        // });
        // });
        for (int y = 0, yOff; y < sequence.getHeight(); y++)
        {
            yOff = y * sequence.getWidth();
            for (int x = 0, pos; x < sequence.getWidth(); x++)
            {
                pos = x + yOff;
                int sp = clusters[pos];
                double[] lab = getCIELab(sequenceData, pos);

                cants[sp]++;

                newls[sp] += (lab[0] - newls[sp]) / cants[sp];
                newas[sp] += (lab[1] - newas[sp]) / cants[sp];
                newbs[sp] += (lab[2] - newbs[sp]) / cants[sp];
                newcxs[sp] += (x - newcxs[sp]) / cants[sp];
                newcys[sp] += (y - newcys[sp]) / cants[sp];
            }
        }

        double error = IntStream.range(0, SPnum).mapToDouble(sp -> {
            // double diffl = ls[sp] - newls[sp];
            // double diffa = as[sp] - newas[sp];
            // double diffb = bs[sp] - newbs[sp];
            double diffx = cxs[sp] - newcxs[sp];
            double diffy = cys[sp] - newcys[sp];

            ls[sp] = newls[sp];
            as[sp] = newas[sp];
            bs[sp] = newbs[sp];
            cxs[sp] = newcxs[sp];
            cys[sp] = newcys[sp];

            return Math.sqrt(diffx * diffx + diffy * diffy/* + diffl * diffl + diffa * diffa + diffb * diffb */);
        }).sum() / SPnum;

        return error;
    }

    private void computeSuperpixels(double[][] sequenceData, int[] clusters, double[] ls, double[] as, double[] bs,
            double[] cxs, double[] cys)
    {

        boolean[] visited = new boolean[clusters.length];
        int[] finalClusters = new int[clusters.length];

        List<Point3D.Double> labs = new ArrayList<>(SPnum);
        List<Double> areas = new ArrayList<>(SPnum);
        List<Point> firstPoints = new ArrayList<>(SPnum);

        // fill known clusters
        AtomicInteger usedLabels = new AtomicInteger(0);
        IntStream.range(0, SPnum).forEach(i -> {
            Point3D.Double labCenter = new Point3D.Double();
            AtomicInteger area = new AtomicInteger(0);
            Point p = new Point((int) Math.round(cxs[i]), (int) Math.round(cys[i]));
            int pPos = p.x + p.y * sequence.getWidth();

            if (clusters[pPos] == i && !visited[pPos])
            {
                findAreaAndColor(sequenceData, clusters, finalClusters, visited, p, labCenter, area,
                        usedLabels.getAndIncrement());
                labs.add(labCenter);
                areas.add(area.get() / (double) (S * S));
                firstPoints.add(p);
            }

        });

        int firstUnknownCluster = usedLabels.get();

        // fill independent clusters
        for (int y = 0; y < sequence.getHeight(); y++)
        {
            int yOff = y * sequence.getWidth();
            for (int x = 0; x < sequence.getWidth(); x++)
            {
                int pos = x + yOff;
                if (!visited[pos])
                {
                    Point3D.Double labCenter = new Point3D.Double();
                    AtomicInteger area = new AtomicInteger(0);
                    Point p = new Point(x, y);

                    findAreaAndColor(sequenceData, clusters, finalClusters, visited, p, labCenter, area,
                            usedLabels.getAndIncrement());

                    labs.add(labCenter);
                    areas.add(area.get() / (double) (S * S));
                    firstPoints.add(p);
                }
            }
        }

        // System.out.println("unvisited = " + IntStream.range(0, visited.length).filter(i -> visited[i] == false).sum());

        // find neighbors and merge independent clusters
        boolean[] mergedClusters = new boolean[usedLabels.get()];
        int[] mergedRefs = IntStream.range(0, usedLabels.get()).toArray();
        IntStream.range(firstUnknownCluster, usedLabels.get()).forEach(i -> {
            List<Integer> neighbors = findNeighbors(finalClusters, visited, firstPoints.get(i));
            if (neighbors.size() > 0)
            {
                int bestNeighbour = neighbors.get(0);
                double bestL = Double.MAX_VALUE;

                // boolean found = false;
                for (Integer j : neighbors)
                {
                    if (j < i)
                    {
                        // found = true;
                        double l = computeL(labs, areas, i, j);
                        if (l < bestL)
                        {
                            bestNeighbour = j;
                            bestL = l;
                        }
                    }
                }
                /*
                 * if (!found) { for (Integer j: neighbors) { double l = computeL(labs,
                 * areas, i, j); if (l < bestL) { bestNeighbour = j; bestL = l; } } }
                 * 
                 * if (found) {
                 */
                double rArea = areas.get(i);
                double relSPSize = rArea / 4;
                relSPSize *= relSPSize;

                double coeff = relSPSize * (1.0 + bestL);
                if (coeff < EPSILON)
                {
                    mergedClusters[i] = true;
                    mergedRefs[i] = bestNeighbour;
                }
                // } else {
                // mergedClusters[i] = true;
                // mergedRefs[i] = bestNeighbour;
                // }
            }
            else
            {
                System.err.format("Cluster at (%d, %d) has no neighbors", firstPoints.get(i).x, firstPoints.get(i).y);
            }
        });
        IntStream.range(firstUnknownCluster, usedLabels.get()).forEach(i -> {
            if (mergedClusters[i])
            {
                int appliedLabel = i;
                while (appliedLabel != mergedRefs[appliedLabel])
                {
                    appliedLabel = mergedRefs[appliedLabel];
                }
                findAreaAndColor(sequenceData, clusters, finalClusters, visited, firstPoints.get(i),
                        new Point3D.Double(), new AtomicInteger(0), appliedLabel);
            }

        });

        if (this.computeROIs)
        {
            // Create and add ROIs to sequence
            this.rois = new ArrayList<>();
            IntStream.range(0, usedLabels.get()).forEach(i -> {
                if (!mergedClusters[i])
                {
                    ROI2DArea roi = defineROI(finalClusters, firstPoints.get(i), labs.get(i));
                    rois.add(roi);
                }
            });
            sequence.addROIs(rois, false);
        }
        else
        {
            List<int[]> rgbs = labs.stream().map(lab -> CIELab.toRGB(lab.x, lab.y, lab.z)).collect(Collectors.toList());
            superPixelsResult = new Sequence(
                    new IcyBufferedImage(sequence.getWidth(), sequence.getHeight(), 3, DataType.UBYTE));

            superPixelsResult.setPixelSizeX(sequence.getPixelSizeX());
            superPixelsResult.setPixelSizeY(sequence.getPixelSizeY());
            superPixelsResult.setPixelSizeZ(sequence.getPixelSizeZ());
            superPixelsResult.setPositionX(sequence.getPositionX());
            superPixelsResult.setPositionY(sequence.getPositionY());
            superPixelsResult.setPositionZ(sequence.getPositionZ());

            superPixelsResult.beginUpdate();
            double[][] spData = Array2DUtil.arrayToDoubleArray(superPixelsResult.getDataXYC(0, 0),
                    superPixelsResult.isSignedDataType());
            for (int y = 0, yOff; y < sequence.getHeight(); y++)
            {
                yOff = y * sequence.getWidth();
                for (int x = 0; x < sequence.getWidth(); x++)
                {
                    final int pos = x + yOff;
                    int[] rgbVal = rgbs.get(finalClusters[pos]);

                    IntStream.range(0, 3).forEach(c -> {
                        spData[c][pos] = rgbVal[c];
                    });

                }
            }
            Array2DUtil.doubleArrayToArray(spData, superPixelsResult.getDataXYC(0, 0));
            superPixelsResult.dataChanged();
            superPixelsResult.endUpdate();
        }
    }

    private void findAreaAndColor(double[][] sequenceData, int[] clusters, int[] newClusters, boolean[] visited,
            Point p, Point3D.Double labCenter, AtomicInteger area, int label)
    {
        int posp = p.x + p.y * sequence.getWidth();
        area.set(0);
        labCenter.x = 0d;
        labCenter.y = 0d;
        labCenter.z = 0d;

        Deque<Point> q = new LinkedList<>();
        int val = clusters[posp];

        visited[posp] = true;
        q.add(p);

        while (!q.isEmpty())
        {
            Point pti = q.pop();
            int posi = pti.x + pti.y * sequence.getWidth();

            newClusters[posi] = label;
            area.getAndIncrement();
            double[] labi = getCIELab(sequenceData, posi);
            labCenter.x += (labi[0] - labCenter.x) / area.get();
            labCenter.y += (labi[1] - labCenter.y) / area.get();
            labCenter.z += (labi[2] - labCenter.z) / area.get();

            int[] ds = new int[] {0, -1, 0, 1, 0};
            for (int is = 1; is < ds.length; is++)
            {
                Point ptn = new Point(pti.x + ds[is - 1], pti.y + ds[is]);
                int posn = ptn.x + ptn.y * sequence.getWidth();
                if (sequence.getBounds2D().contains(ptn.x, ptn.y) && !visited[posn] && clusters[posn] == val)
                {
                    visited[posn] = true;
                    q.add(ptn);
                }
            }

        }
    }

    private List<Integer> findNeighbors(int[] newClusters, boolean[] visited, Point p)
    {
        int posp = p.x + p.y * sequence.getWidth();

        HashSet<Integer> neighs = new HashSet<>();

        Deque<Point> q = new LinkedList<>();
        int val = newClusters[posp];

        visited[posp] = false;
        q.add(p);

        while (!q.isEmpty())
        {
            Point pti = q.pop();

            int[] ds = new int[] {0, -1, 0, 1, 0};
            for (int is = 1; is < ds.length; is++)
            {
                Point ptn = new Point(pti.x + ds[is - 1], pti.y + ds[is]);
                int posn = ptn.x + ptn.y * sequence.getWidth();
                if (sequence.getBounds2D().contains(ptn.x, ptn.y))
                {
                    if (newClusters[posn] == val)
                    {
                        if (visited[posn])
                        {
                            visited[posn] = false;
                            q.add(ptn);
                        }
                    }
                    else
                    {
                        neighs.add(newClusters[posn]);
                    }
                }
            }

        }
        return new ArrayList<Integer>(neighs);
    }

    private double computeL(List<Point3D.Double> labs, List<Double> areas, int i, Integer j)
    {
        Point3D.Double diffLab = new Point3D.Double();
        diffLab.x = labs.get(j).x - labs.get(i).x;
        diffLab.y = labs.get(j).y - labs.get(i).y;
        diffLab.z = labs.get(j).z - labs.get(i).z;

        try
        {
            return diffLab.length() / areas.get(j);
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    private ROI2DArea defineROI(int[] newClusters, Point p, Point3D.Double labP)
    {
        int posp = p.x + p.y * sequence.getWidth();
        double[] lab = new double[] {labP.x, labP.y, labP.z};
        int[] rgb = CIELab.toRGB(lab);

        int val = newClusters[posp];
        Rectangle seqBounds = sequence.getBounds2D();
        ROI2DArea roi1 = new ROI2DArea(new BooleanMask2D());
        IntStream.range(0, newClusters.length).filter(pi -> newClusters[pi] == val)
                .forEach(pi -> roi1.addPoint(pi % seqBounds.width, pi / seqBounds.width));// .forEach(pi -> bMask[pi] = true);

        roi1.setColor(new Color(rgb[0], rgb[1], rgb[2]));
        return roi1;
    }

    public Sequence getResultSequence()
    {
        return this.superPixelsResult;
    }
}
