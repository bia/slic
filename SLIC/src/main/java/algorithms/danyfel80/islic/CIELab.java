package algorithms.danyfel80.islic;

public class CIELab {

	public static class XYZ {

		private static final double[][]	M		= { { 0.4124, 0.3576, 0.1805 }, { 0.2126, 0.7152, 0.0722 },
				{ 0.0193, 0.1192, 0.9505 } };
		private static final double[][]	Mi	= { { 3.2406, -1.5372, -0.4986 }, { -0.9689, 1.8758, 0.0415 },
				{ 0.0557, -0.2040, 1.0570 } };

		public static double[] fromRGB(int[] rgb) {
			return fromRGB(rgb[0], rgb[1], rgb[2]);
		}

		public static double[] fromRGB(int R, int G, int B) {
			double[] result = new double[3];

			// convert 0..255 into 0..1
			double r = R / 255.0;
			double g = G / 255.0;
			double b = B / 255.0;

			// assume sRGB
			if (r <= 0.04045) {
				r = r / 12.92;
			} else {
				r = Math.pow(((r + 0.055) / 1.055), 2.4);
			}
			if (g <= 0.04045) {
				g = g / 12.92;
			} else {
				g = Math.pow(((g + 0.055) / 1.055), 2.4);
			}
			if (b <= 0.04045) {
				b = b / 12.92;
			} else {
				b = Math.pow(((b + 0.055) / 1.055), 2.4);
			}

			r *= 100.0;
			g *= 100.0;
			b *= 100.0;

			// [X Y Z] = [r g b][M]
			result[0] = (r * M[0][0]) + (g * M[0][1]) + (b * M[0][2]);
			result[1] = (r * M[1][0]) + (g * M[1][1]) + (b * M[1][2]);
			result[2] = (r * M[2][0]) + (g * M[2][1]) + (b * M[2][2]);

			return result;
		}

		public static int[] toRGB(double[] xyz) {
			return toRGB(xyz[0], xyz[1], xyz[2]);
		}

		public static int[] toRGB(double X, double Y, double Z) {
			int[] result = new int[3];

			double x = X / 100.0;
			double y = Y / 100.0;
			double z = Z / 100.0;

			// [r g b] = [X Y Z][Mi]
			double r = (x * Mi[0][0]) + (y * Mi[0][1]) + (z * Mi[0][2]);
			double g = (x * Mi[1][0]) + (y * Mi[1][1]) + (z * Mi[1][2]);
			double b = (x * Mi[2][0]) + (y * Mi[2][1]) + (z * Mi[2][2]);

			// assume sRGB
			if (r > 0.0031308) {
				r = ((1.055 * Math.pow(r, 1.0 / 2.4)) - 0.055);
			} else {
				r = (r * 12.92);
			}
			if (g > 0.0031308) {
				g = ((1.055 * Math.pow(g, 1.0 / 2.4)) - 0.055);
			} else {
				g = (g * 12.92);
			}
			if (b > 0.0031308) {
				b = ((1.055 * Math.pow(b, 1.0 / 2.4)) - 0.055);
			} else {
				b = (b * 12.92);
			}

			r = (r < 0) ? 0 : r;
			g = (g < 0) ? 0 : g;
			b = (b < 0) ? 0 : b;

			// convert 0..1 into 0..255
			result[0] = (int) Math.round(r * 255);
			result[1] = (int) Math.round(g * 255);
			result[2] = (int) Math.round(b * 255);

			return result;
		}

	}

	private static final double[] whitePoint = { 95.0429, 100.0, 108.8900 }; // D65

	public static double[] fromRGB(int[] rgb) {
		return fromXYZ(XYZ.fromRGB(rgb));
	}

	public static double[] fromRGB(int r, int g, int b) {
		return fromXYZ(XYZ.fromRGB(r, g, b));
	}

	private static double[] fromXYZ(double[] xyz) {
		return fromXYZ(xyz[0], xyz[1], xyz[2]);
	}

	public static double[] fromXYZ(double X, double Y, double Z) {
		double x = X / whitePoint[0];
    double y = Y / whitePoint[1];
    double z = Z / whitePoint[2];

    if (x > 0.008856) {
      x = Math.pow(x, 1.0 / 3.0);
    }
    else {
      x = (7.787 * x) + (16.0 / 116.0);
    }
    if (y > 0.008856) {
      y = Math.pow(y, 1.0 / 3.0);
    }
    else {
      y = (7.787 * y) + (16.0 / 116.0);
    }
    if (z > 0.008856) {
      z = Math.pow(z, 1.0 / 3.0);
    }
    else {
      z = (7.787 * z) + (16.0 / 116.0);
    }

    double[] result = new double[3];

    result[0] = (116.0 * y) - 16.0;
    result[1] = 500.0 * (x - y);
    result[2] = 200.0 * (y - z);

    return result;
	}
	
	public static int[] toRGB(double[] lab) {
		return XYZ.toRGB(toXYZ(lab));
	}

	public static int[] toRGB(double L, double a, double b) {
		return XYZ.toRGB(toXYZ(L, a, b));
	}
	
	public static double[] toXYZ(double[] lab) {
		return toXYZ(lab[0], lab[1], lab[2]);
	}

	public static double[] toXYZ(double L, double a, double b) {
		double[] result = new double[3];

		double y = (L + 16.0) / 116.0;
		double y3 = Math.pow(y, 3.0);
		double x = (a / 500.0) + y;
		double x3 = Math.pow(x, 3.0);
		double z = y - (b / 200.0);
		double z3 = Math.pow(z, 3.0);

		if (y3 > 0.008856) {
			y = y3;
		} else {
			y = (y - (16.0 / 116.0)) / 7.787;
		}
		if (x3 > 0.008856) {
			x = x3;
		} else {
			x = (x - (16.0 / 116.0)) / 7.787;
		}
		if (z3 > 0.008856) {
			z = z3;
		} else {
			z = (z - (16.0 / 116.0)) / 7.787;
		}

		// results in 0...100
		result[0] = x * whitePoint[0];
		result[1] = y * whitePoint[1];
		result[2] = z * whitePoint[2];

		return result;
	}
}
