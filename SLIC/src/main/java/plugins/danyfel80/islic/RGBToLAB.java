package plugins.danyfel80.islic;

import java.util.stream.IntStream;

import algorithms.danyfel80.islic.CIELab;
import icy.image.IcyBufferedImage;
import icy.sequence.Sequence;
import icy.type.DataType;
import icy.type.collection.array.Array2DUtil;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarSequence;

public class RGBToLAB extends EzPlug {

	EzVarSequence inRgbSequence;

	@Override
	protected void initialize() {
		inRgbSequence = new EzVarSequence("RGB sequence");
		addEzComponent(inRgbSequence);
	}

	@Override
	protected void execute() {

		Sequence rgbSequence = inRgbSequence.getValue();
		Sequence labSequence = new Sequence(
				new IcyBufferedImage(rgbSequence.getWidth(), rgbSequence.getHeight(), 3, DataType.DOUBLE));

		double[][] rgbIm = Array2DUtil.arrayToDoubleArray(rgbSequence.getDataXYC(0, 0), rgbSequence.isSignedDataType());

		labSequence.beginUpdate();
		double[][] labIm = Array2DUtil.arrayToDoubleArray(labSequence.getDataXYC(0, 0), labSequence.isSignedDataType());
		IntStream.range(0, labIm[0].length).forEach(pos -> {
			int[] rgb = IntStream.range(0, 3).map(c -> (int) Math.round(255 * (rgbIm[c][pos] / rgbSequence.getDataTypeMax())))
					.toArray();
			double[] lab = CIELab.fromRGB(rgb);
			IntStream.range(0, 3).forEach(c -> labIm[c][pos] = lab[c]);
		});
		Array2DUtil.doubleArrayToArray(labIm, labSequence.getDataXYC(0, 0));
		labSequence.dataChanged();
		labSequence.endUpdate();

		labSequence.setName(rgbSequence.getName() + "_LAB");
		addSequence(labSequence);
	}

	@Override
	public void clean() {}

}
