package plugins.danyfel80.islic;

import java.util.stream.IntStream;

import algorithms.danyfel80.islic.CIELab;
import icy.image.IcyBufferedImage;
import icy.sequence.Sequence;
import icy.type.DataType;
import icy.type.collection.array.Array2DUtil;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarSequence;

public class LABToRGB extends EzPlug {
	
	EzVarSequence inLabSequence;

	@Override
	protected void initialize() {
		inLabSequence = new EzVarSequence("LAB sequence");
		addEzComponent(inLabSequence);
	}

	@Override
	protected void execute() {
		Sequence labSequence = inLabSequence.getValue();
		Sequence rgbSequence = new Sequence(new IcyBufferedImage(labSequence.getWidth(), labSequence.getHeight(),3, DataType.UBYTE));
		
		double[][] labIm = Array2DUtil.arrayToDoubleArray(labSequence.getDataXYC(0, 0), labSequence.isSignedDataType());
		
		rgbSequence.beginUpdate();
		double[][] rgbIm = Array2DUtil.arrayToDoubleArray(rgbSequence.getDataXYC(0, 0), rgbSequence.isSignedDataType());
		IntStream.range(0, rgbIm[0].length).forEach(pos->{
			double[] lab = IntStream.range(0, 3).mapToDouble(c->labIm[c][pos]).toArray();
			int[] rgb = CIELab.toRGB(lab);
			IntStream.range(0, 3).forEach(c->rgbIm[c][pos] = rgb[c]);
		});
		Array2DUtil.doubleArrayToArray(rgbIm, rgbSequence.getDataXYC(0, 0));
		rgbSequence.dataChanged();
		rgbSequence.endUpdate();
		
		
		addSequence(rgbSequence);
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub
	}

}
