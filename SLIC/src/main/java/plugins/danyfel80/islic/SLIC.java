package plugins.danyfel80.islic;

import algorithms.danyfel80.islic.SLICTask;
import icy.gui.dialog.MessageDialog;
import icy.main.Icy;
import icy.plugin.PluginLauncher;
import icy.plugin.PluginLoader;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarDouble;
import plugins.adufour.ezplug.EzVarInteger;
import plugins.adufour.ezplug.EzVarSequence;

public class SLIC extends EzPlug implements Block
{

    private EzVarSequence inSequence;
    private EzVarInteger inSPSize;
    private EzVarDouble inSPReg;
    private EzVarBoolean inIsROIOutput;

    private EzVarSequence outSequence;

    @Override
    protected void initialize()
    {
        inSequence = new EzVarSequence("Sequence");
        inSPSize = new EzVarInteger("Superpixel size");
        inSPReg = new EzVarDouble("Superpixels regularity");
        inIsROIOutput = new EzVarBoolean("Output as ROIs", false);

        inSPSize.setValue(30);
        inSPReg.setValue(0.2);

        addEzComponent(inSequence);
        addEzComponent(inSPSize);
        addEzComponent(inSPReg);
        addEzComponent(inIsROIOutput);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inSequence = new EzVarSequence("Sequence");
        inSPSize = new EzVarInteger("Superpixel size");
        inSPReg = new EzVarDouble("Superpixels regularity");
        inIsROIOutput = new EzVarBoolean("Output as ROIs", false);

        inSPSize.setValue(30);
        inSPReg.setValue(0.2);

        inputMap.add(inSequence.name, inSequence.getVariable());
        inputMap.add(inSPSize.name, inSPSize.getVariable());
        inputMap.add(inSPReg.name, inSPReg.getVariable());
        inputMap.add(inIsROIOutput.name, inIsROIOutput.getVariable());
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outSequence = new EzVarSequence("Result");

        outputMap.add(outSequence.name, outSequence.getVariable());
    }

    @Override
    protected void execute()
    {
        long procTime, startTime, endTime;
        SLICTask task;
        try
        {
            task = new SLICTask(inSequence.getValue(), inSPSize.getValue(), inSPReg.getValue(),
                    inIsROIOutput.getValue());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            if (!this.isHeadLess())
            {
                MessageDialog.showDialog("Initialization Error",
                        String.format("SLIC could not start properly: " + e.getMessage()), MessageDialog.ERROR_MESSAGE);
            }
            return;
        }
        startTime = System.currentTimeMillis();
        try
        {
            task.execute();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            if (!this.isHeadLess())
            {
                MessageDialog.showDialog("Runtime Error",
                        String.format("SLIC could not run properly: " + e.getMessage()), MessageDialog.ERROR_MESSAGE);
            }
            return;
        }

        endTime = System.currentTimeMillis();
        procTime = endTime - startTime;
        System.out.println(String.format("SLIC finished in %d milliseconds", procTime));
        if (!this.inIsROIOutput.getValue())
        {
            task.getResultSequence().setName(inSequence.getValue().getName() + String.format("_SLIC(size=%s,reg=%.2f)",
                    inSPSize.getValue().intValue(), inSPReg.getValue().doubleValue()));
        }
        if (!this.isHeadLess())
        {
            MessageDialog.showDialog(String.format("SLIC finished in %d milliseconds", procTime));
            addSequence(task.getResultSequence());
        }
        else
        {
            outSequence.setValue(task.getResultSequence());
        }

    }

    @Override
    public void clean()
    {
        // TODO Auto-generated method stub
    }

    public static void main(String[] args)
    {
        Icy.main(args);
        PluginLauncher.start(PluginLoader.getPlugin(SLIC.class.getName()));
    }
}
